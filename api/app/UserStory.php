<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStory extends Model
{
    protected $fillable = ['name', 'size', 'project_id'];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function sprint()
    {
        return $this->belongsTo('App\Sprint');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
