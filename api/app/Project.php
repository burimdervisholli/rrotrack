<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['name', 'deadline', 'logo'];

    public function userStories()
    {
        return $this->hasMany('App\UserStory');
    }

    public function sprints()
    {
        return $this->hasMany('App\Sprint');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
