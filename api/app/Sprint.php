<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sprint extends Model
{
    protected $fillable = ['name', 'start_date', 'end_date', 'project_id'];
    public $timestamps = false;

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function userStories()
    {
        return $this->hasMany('App\UserStory');
    }

    public function columns()
    {
        return $this->hasMany('App\Column');
    }
}
