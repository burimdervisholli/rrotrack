<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Column extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;

    public function sprint()
    {
        return $this->belongsTo('App\Sprint');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
