<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name', 'description', 'estimate', 'image', 'user_id', 'task_role_id', 'column_id', 'user_story_id', 'project_id'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function taskRole()
    {
        return $this->belongsTo('App\TaskRole');
    }

    public function column()
    {
        return $this->belongsTo('App\Column');
    }

    public function userStory()
    {
        return $this->belongsTo('App\UserStory');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
