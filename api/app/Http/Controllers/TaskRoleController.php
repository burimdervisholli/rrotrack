<?php

namespace App\Http\Controllers;

use App\TaskRole;
use Illuminate\Http\Request;

class TaskRoleController extends Controller
{
    public function index()
    {
        $taskRoles = TaskRole::all();

        return response()->json(['task_roles' => $taskRoles], 200);        
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $taskRole = TaskRole::create([
            'name'  => $request->name
        ]);

        return response()->json(['message' => 'Task role created successfully.'], 200);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $taskRole = TaskRole::findOrFail($id);
        $taskRole->name = $request->name;

        return response()->json(['message' => 'Task role created successfully.'], 200);
    }

    public function destroy($id)
    {
        $taskRole = TaskRole::findOrFail($id);
        $taskRole->delete();

        return response()->json(['message' => 'Task role has been deleted.'], 200);
    }
}
