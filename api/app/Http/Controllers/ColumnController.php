<?php

namespace App\Http\Controllers;

use App\Column;
use Illuminate\Http\Request;

class ColumnController extends Controller
{
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $column = Column::create([
            'name'      => $request->name,
            'sprint_id' => $request->sprint_id
        ]);
 
        return response()->json(['message' => 'Column created successfully.'], 200);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $column = Column::findOrFail($id);

        $column->name = $request->name;
        $column->sprint_id = $request->sprint_id;
        $column->save();

        return response()->json(['message' => 'Column updated successfully.'], 200);
    }

    public function destroy($id)
    {
        
    }
}
