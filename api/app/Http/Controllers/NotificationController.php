<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();

        $notifications = Notification::where('user_id', $user->id)->orderBy('has_viewed', 'asc')->orderBy('created_at', 'asc')->get();

        return response()->json(['notifications' => $notifications], 200);
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function markAsRead(Request $request)
    {
        $user = $request->user();
        foreach($request->notificationIds as $id)
        {
            $notification = Notification::where('user_id', $user->id)->where('id', $id)->firstOrFail();
            
            if(!$notification)
            {
                return response()->json(['message' => 'Not authorized.'], 401);       
            }

            $notification->has_viewed = 1;
            $notification->save();
        }

        $notifications = Notification::where('user_id', $user->id)->orderBy('has_viewed', 'asc')->orderBy('created_at', 'asc')->get();

        return response()->json(['notifications' => $notifications], 200);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
