<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        return response()->json(['roles' => $roles], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $role = Role::create([
            'name'  => $request->name
        ]);

        return response()->json(['message' => 'Role created successfully.'], 200);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $role = Role::findOrFail($id);

        $role->name = $request->name;
        $role->save();

        return response()->json(['message' => 'Role updated successfully.'], 200);
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return response()->json(['message' => 'Role has been deleted.'], 200);
    }
}
