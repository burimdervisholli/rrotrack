<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use App\UserStory;
use App\Sprint;
use App\Task;
use App\Http\Resources\Project as ProjectResource;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::all();
        
        return response()->json(['projects' => $projects], 200);
    }

    public function stats()
    {
        $projects = Project::all();
        $users = User::all();
        $userStories = UserStory::all();
        $sprints = Sprint::all();
        $tasks = Task::all();

        return response()->json([
            'projects' => count($projects),
            'users' => count($users),
            'userStories' => count($userStories),
            'sprints' => count($sprints),
            'tasks' => count($tasks)
        ], 200);
    }

    public function collection()
    {
        $projects = ProjectResource::collection(Project::all());

        return response()->json(['projects' => $projects], 200);
    }

    public function latest()
    {
        $projects = Project::orderBy('id', 'desc')->take(5)->get();
        
        foreach($projects as $project)
        {
            $deadline = new Carbon($project->deadline);
            $project->deadline = $deadline->toFormattedDateString();
        }

        return response()->json(['projects' => $projects], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);        

        $logo = null;
        if($request->hasFile('logo'))
        {
            $file = $request->logo;
            $logo = uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/project_logos/'), $logo);
        }

        $project = Project::create([
            'name'      => $request->name,
            'deadline'  => $request->deadline,
            'logo'      => $logo
        ]);

        return response()->json(['message' => 'Project created successfully.'], 200);
    }

    public function show($id)
    {
        $project = Project::findOrFail($id);

        $projectResource = new ProjectResource($project);

        return response()->json(['project' => $projectResource], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $project = Project::findOrFail($id);

        $project->name = $request->name;
        $project->deadline = $request->deadline;
        
        if($request->hasFile('logo'))
        {
            if($project->logo != null && file_exists($project->logo))
            {
                unlink($project->logo);
            }

            $file = $request->logo;
            $logo = uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/project_logos/'), $logo);
            $project->logo = $logo;
        }

        $project->save();

        $projectResource = new ProjectResource($project);

        return response()->json([
            'project' => $projectResource,
            'message' => 'Project updated successfully.'
        ], 200);
    }

    public function notAttachedUsers($id)
    {
        $project = Project::findOrFail($id);

        $notAttachedUsers = User::whereDoesntHave('projects', function($q) use ($project){
            $q->where('project_id', $project->id);
        })->get();

        $users = UserResource::collection($notAttachedUsers);

        return response()->json(['users' => $users], 200);
    }

    public function attachUsers(Request $request, $projectId)
    {
        $project = Project::findOrFail($projectId);
        $project->users()->attach($request->users);

        $users = UserResource::collection($project->users);

        return response()->json([
            'users'     => $users,
            'message'   => 'User added successfully.'
        ], 200);
    }

    public function detachUser(Request $request, $projectId)
    {
        $project = Project::findOrFail($projectId);
        $project->users()->detach($request->user_id);

        return response()->json(['message' => 'The user has been removed.'], 200);
    }

    public function destroy($id)
    {
        //
    }
}
