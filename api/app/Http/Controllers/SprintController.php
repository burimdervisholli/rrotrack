<?php

namespace App\Http\Controllers;

use App\Sprint;
use App\Column;
use App\Task;
use App\UserStory;
use App\Http\Resources\Sprint as SprintResource;
use Illuminate\Http\Request;

class SprintController extends Controller
{
    public function index()
    {
        $sprint = Sprint::all();

        return response()->json(['sprint' => $sprint], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'end_date'      => 'required',
            'project_id'    => 'required'
        ]);

        $sprint = Sprint::create([
            'name'              => $request->name,
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date,
            'project_id'        => $request->project_id,
        ]);

        $sprint = new SprintResource($sprint);

        return response()->json([
            'sprint'    => $sprint,
            'message'   => 'Sprint created successfully.'
        ], 200);
    }

    public function show($id)
    {
        $sprint = Sprint::findOrFail($id);

        $sprintResource = new SprintResource($sprint);

        return response()->json(['sprint' => $sprintResource], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required',
            'end_date'      => 'required',
        ]);

        $sprint = Sprint::findOrFail($id);

        $sprint->name = $request->name;
        $sprint->start_date = $request->start_date;
        $sprint->end_date = $request->end_date;
        $sprint->save();

        return response()->json([
            'sprint'    => $sprint,
            'message'   => 'Sprint updated successfully.'
        ], 200);
    }

    public function notAttachedUserstories($sprintId) 
    {
        $sprint = Sprint::findOrFail($sprintId);

        $userStories = UserStory::whereDoesntHave('sprint', function($q) use ($sprint){
            $q->where('sprint_id', $sprint->id);
        })->get();

        return response()->json(['user_stories' => $userStories], 200);
    }

    public function sprintAddUserstories(Request $request, $sprintId)
    {
        $userstories = $request->user_stories;

        foreach($userstories as $id)
        {
            $userStory = UserStory::findOrFail($id);
            $userStory->sprint_id = $sprintId;
            $userStory->save();
        }

        $sprint = Sprint::findOrFail($sprintId);

        $sprintResource = new SprintResource($sprint);

        return response()->json([
            'sprint'    => $sprintResource,
            'message'   => 'User stories have been added successfully.'
        ], 200);
    }

    public function destroy($id)
    {
        $sprint = Sprint::findOrFail($id);
        $sprint->delete();

        return response()->json(['message' => 'Sprint has been removed.'], 200);
    }
}
