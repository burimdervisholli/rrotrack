<?php

namespace App\Http\Controllers;

use App\Task;
use App\Http\Resources\Task as TaskResource;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'estimate'      => 'required',
            'task_role_id'  => 'required'
        ]);

        $image = null;

        if($request->hasFile('image'))
        {
            $file = $request->image;
            $image = uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/task_images/'), $image);
        }

        $task = Task::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'estimate'      => $request->estimate,
            'image'         => $image,
            'user_id'       => $request->user_id,
            'project_id'    => $request->project_id,
            'task_role_id'  => $request->task_role_id,
            'column_id'     => $request->column_id,
            'user_story_id' => $request->user_story_id
        ]);

        $taskResource = new TaskResource($task);

        return response()->json([
            'task'      => $taskResource,
            'message'   => 'Task created successfully.'
        ], 200);
    }

    public function show($id)
    {
        $task = Task::findOrFail($id);

        $taskResource = new TaskResource($task);

        return response()->json(['task' => $taskResource], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required',
            'estimate'      => 'required',
            'task_role_id'  => 'required',
        ]);

        $task = Task::findOrFail($id);
        
        $task->name = $request->name;
        $task->description = $request->description;
        $task->estimate = $request->estimate;
        $task->user_id = $request->user_id;
        $task->task_role_id = $request->task_role_id;

        if($request->hasFile('image'))
        {
            if($task->image != null && file_exists($task->image))
            {
                unlink($task->image);
            }

            $file = $request->image;
            $image = uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/task_images/'), $image);
            $task->image = $image;
        }

        $task->save();

        return response()->json(['message' => 'Task updated successfully.'], 200);
    }

    public function updateTaskColumn(Request $request, $id)
    {
        $this->validate($request, [
            'column_id' => 'required'
        ]);

        $task = Task::findOrFail($id);

        $task->column_id = $request->column_id;

        return response()->json(['message' => 'Task column update successfully.'], 200);
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);

        if($task->image != null && file_exists($task->image))
        {
            unlink($task->image);
        }

        $task->delete();

        return response()->json(['message' => 'Task has been deleted.'], 200);
    }
}
