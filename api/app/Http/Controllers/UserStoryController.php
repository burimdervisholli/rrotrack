<?php

namespace App\Http\Controllers;

use App\UserStory;
use App\Http\Resources\UserStory as UserStoryResource;
use Illuminate\Http\Request;

class UserStoryController extends Controller
{
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'project_id'    => 'required'
        ]);

        $userStory = UserStory::create([
            'name'          => $request->name,
            'size'          => $request->size,
            'project_id'    => $request->project_id,
            'sprint_id'     => $request->sprint_id
        ]);

        return response()->json([
            'user_story'     => $userStory,
            'message'   => 'User story created successfully.'
        ], 200);
    }

    public function show($id)
    {
        $userStory = UserStory::findOrFail($id);

        $userStoryResource = new UserStoryResource($userStory);

        return response()->json(['story' => $userStoryResource], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required',
            'project_id'    => 'required'
        ]);

        $userStory = UserStory::findOrFail($id);

        $userStory->name = $request->name;
        $userStory->size = $request->size;
        $userStory->project_id = $request->project_id;
        $userStory->sprint_id = $request->sprint_id;
        $userStory->save();

        return response()->json(['message' => 'User story updated successfully.'], 200);
    }

    public function destroy($id)
    {
        $userStory = UserStory::findOrFail($id);
        $userStory->delete();

        return response()->json(['message' => 'User story has been deleted.'], 200);
    }
}
