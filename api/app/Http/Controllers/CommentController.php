<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Request;

class CommentController extends Controller
{    
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'text' => 'required|max:500'
        ]);

        $image = null;

        if($request->hasFile('image'))
        {
            $file = $request->image;
            $image = uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/comment_images/'), $image);
        }

        $comment = Comment::create([
            'text'      => $request->text,
            'user_id'   => $request->user()->id,
            'task_id'   => $request->task_id,
            'image'     => $image
        ]);

        $commentResource = new CommentResource($comment);

        return response()->json([
            'comment'   => $commentResource,
            'message'   => 'Successfully commented'
        ], 200);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::findOrFail($id);

        if($request->user()->id == $comment->user->id) 
        {
            $this->validate($request, [
                'text' => 'required|max:200'
            ]);
            
            $comment->text = $request->text;
            $comment->task_id = $request->task_id;
            $comment->save();

            return response()->json([
                'comment' => $comment,
                'message' => 'Comment updated successfully'
            ], 200);
        }
        else
        {
            return response()->json(['message' => 'Not authorized'], 401);
        }
    }

    public function destroy(Request $request, $id)
    {
        $comment = Comment::findOrFail($id);

        if($request->user()->id == $comment->user->id) 
        {
            $comment->delete();
            return response()->json(['message' => 'Comment updated successfully'], 200);
        }
        else
        {
            return response()->json(['message' => 'Not authorized'], 401);
        }
    }
}
