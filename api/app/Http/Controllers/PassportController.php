<?php
 
namespace App\Http\Controllers;
 
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
 
class PassportController extends Controller
{
    public function index()
    {
        $users = User::all();

        $usersResource = UserResource::collection($users);

        return response()->json(['users' => $usersResource], 200);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);
 
        $user = User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'bio'       => $request->bio,
            'role_id'   => $request->role_id
        ]);
 
        return response()->json(['message' => 'User created successfully.'], 200);
    }

    public function updateProfile(Request $request, $id)
    {
        if($request->user()->id == $id)
        {
            $this->validate($request, [
                'name' => 'required|min:3',
                'email' => 'unique:users,email,'.$id
            ]);

            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->bio = $request->bio;
            $user->email = $request->email;

            if($request->hasFile('avatar'))
            {
                if(file_exists($user->avatar))
                {
                    File::delete($user->avatar);
                }

                $file = $request->avatar;
                $avatarFileName = uniqid() . '.' . $file->getClientOriginalExtension();

                $file->move(public_path('uploads/avatars/'), $avatarFileName); 
                $user->avatar = $avatarFileName;               
            }
            if($request->hasFile('cover'))
            {
                if(file_exists($user->cover))
                {
                    File::delete($user->cover);
                }

                $file = $request->cover;
                $coverFileName = uniqid() . '.' . $file->getClientOriginalExtension();

                $file->move(public_path('uploads/covers/'), $coverFileName); 
                $user->cover = $coverFileName;
            }
            $user->save();

            return response()->json([
                'user'      => $user,
                'message'   => 'Your profile has been update'
            ], 200);
        }
        else
        {
            return response()->json(['message' => 'Not authorized'], 401);
        }
    }

    public function updatePassword(Request $request, $id)
    {
        if($request->user()->id == $id)
        {
            $user = User::findOrFail($id);

            $this->validate($request, [
                'password' => 'required',
                'confirm_password' => 'required|min:6|same:password',
            ]);

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['message' => 'Password changed successfully'], 200);
        }
        else
        {
            return response()->json(['message' => 'Not authorized'], 401);
        }
    }
 
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('rrotrackToken')->accessToken;
            $user = new UserResource(auth()->user());
            return response()->json([
                'token' => $token,
                'user'  => $user
            ], 200);
        } else {
            return response()->json(['error' => 'Not authorized'], 401);
        }
    }
 
    public function details()
    {
        $user = new UserResource(auth()->user());
        
        return response()->json(['user' => $user ], 200);
    }

    public function show(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $self = false;

        if($request->user()->id == $id)
        {
            $self = true;
        }

        $user = new UserResource($user);

        return response()->json([
            'user' => $user,
            'self' => $self,
        ], 200);
    }

    public function logout()
    {
        $token = auth()->user()->token();
        $token->revoke();

        return response()->json([
            'message'   => 'You have been succesfully logged out!',
        ], 200);
    }

    public function delete($id)
    {    
        $authUser = auth()->user();
        
        if($authUser->role->name == 'Administrator') {
            $user = User::findOrFail($id);
            $user->delete();

            return response()->json(['message' => 'User successfully deleted.'], 200);
        }

        return response()->json(['error' => 'Not authorized'], 401);
    }
}