<?php

namespace App\Http\Resources;

use App\Http\Resources\Sprint as SprintResource;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class Project extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $createdAt = new Carbon($this->created_at);
        $deadline = new Carbon($this->deadline);

        $sprints = SprintResource::collection($this->sprints);
        $users = UserResource::collection($this->users);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'logo' => $this->logo,
            'deadline_at' => $this->deadline,
            'deadline' => $deadline->diffForHumans(),
            'created_at' => $createdAt->toFormattedDateString(),
            'updated_at' => $this->updated_at,
            'user_stories' => $this->userStories,
            'sprints' => $sprints,
            'users' => $users,
            'number_of_tasks' => count($this->tasks)
        ];
    }
}
