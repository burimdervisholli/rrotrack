<?php

namespace App\Http\Resources;

use App\Http\Resources\Task as TaskResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class UserStory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tasks = TaskResource::collection($this->tasks);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'size' => $this->size,
            'tasks' => $tasks,
            'project_id' => $this->project->id,
            'project_name' => $this->project->name
        ];
    }
}
