<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $createdAt = new Carbon($this->created_at);
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => $this->avatar,
            'cover' => $this->cover,
            'bio' => $this->bio,
            'created_at' => $createdAt->toFormattedDateString(),
            'role' => $this->role,
            'notifications' => $this->notifications,
            'new_notifications' => count($this->newNotifications)
        ];
    }
}
