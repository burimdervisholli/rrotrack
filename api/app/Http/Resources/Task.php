<?php

namespace App\Http\Resources;

use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class Task extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $comments = CommentResource::collection($this->comments);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'estimate' => $this->estimate,
            'image' => $this->image,
            'role' => $this->taskRole,
            'user' => $this->user,
            'comments' => $comments
        ];
    }
}
