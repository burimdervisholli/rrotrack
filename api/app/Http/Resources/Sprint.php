<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class Sprint extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $now = Carbon::now();
        $start_date = new Carbon($this->start_date);
        $end_date = new Carbon($this->end_date);
        $active = $now->greaterThan($start_date) && $now->lessThanOrEqualTo($end_date);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'active' => $active,
            'start_date' => $start_date->toFormattedDateString(),
            'end_date' => $end_date->toFormattedDateString(),
            'project' => $this->project->id,
            'project_name' => $this->project->name,
            'columns' => $this->columns,
            'user_stories' => $this->userStories
        ];
    }
}
