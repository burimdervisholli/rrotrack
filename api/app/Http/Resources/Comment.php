<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $now = Carbon::now();
        $createAt = new Carbon($this->created_at);

        return [
            'id' => $this->id,
            'text' => $this->text,
            'image' => $this->image,
            'created_at' => $createAt->toFormattedDateString(),
            'user' => $this->user,
            'mentions' => $this->mentions
        ];
    }
}
