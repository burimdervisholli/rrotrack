<?php

use Illuminate\Http\Request;
use App\Http\Resources\Project as ProjectResource;
use App\Http\Resources\ProjectsCollection;
use App\Project;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'PassportController@login');

Route::post('register', 'PassportController@register');
 
Route::middleware('auth:api')->group(function () {

    Route::get('logout', 'PassportController@logout');

    // User routes
    Route::get('user', 'PassportController@details');
    Route::get('users', 'PassportController@index');
    Route::get('users/{id}', 'PassportController@show');
    Route::post('user/update/{id}', 'PassportController@updateProfile');
    Route::post('user/update/password/{id}', 'PassportController@updatePassword');
    Route::get('user/delete/{id}', 'PassportController@delete');
    
    Route::resource('columns', 'ColumnController');
    
    // Project routes
    Route::get('stats', 'ProjectController@stats');
    Route::get('projects/collection', 'ProjectController@collection');
    Route::get('latest/projects', 'ProjectController@latest');
    Route::get('/projects/not-attached-users/{id}', 'ProjectController@notAttachedUsers');
    Route::post('/projects/attach-users/{projectId}', 'ProjectController@attachUsers');
    Route::post('/projects/detach-user/{projectId}', 'ProjectController@detachUser');
    Route::resource('projects', 'ProjectController');

    Route::resource('roles', 'RoleController');
    
    Route::get('/sprints/not-attached-userstories/{sprintId}', 'SprintController@notAttachedUserstories');
    Route::post('/sprints/add-usertories/{sprintId}', 'SprintController@sprintAddUserstories');
    Route::resource('sprints', 'SprintController');
    
    // Task routers
    Route::resource('tasks', 'TaskController');
    Route::post('tasks/update-column/{id}', 'TaskController@updateTaskColumn');
    
    Route::resource('taskroles', 'TaskRoleController');
    
    Route::resource('userstories', 'UserStoryController');

    Route::resource('comments', 'CommentController');
    
    Route::resource('notifications', 'NotificationController');

    Route::post('notifications/mark-as-read/', 'NotificationController@markAsRead');
});