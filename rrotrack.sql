-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2020 at 08:40 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rrotrack`
--

-- --------------------------------------------------------

--
-- Table structure for table `columns`
--

CREATE TABLE `columns` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sprint_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) NOT NULL,
  `text` text NOT NULL,
  `image` varchar(155) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `task_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `text`, `image`, `created_at`, `updated_at`, `user_id`, `task_id`) VALUES
(14, 'Nice dinner with best team ever', '5e47f99c6d0ed.jpeg', '2020-02-15 13:01:00', '2020-02-15 13:01:00', 4, 21),
(15, 'Lorem ipsum dolor', '5e47fa6164dbd.jpeg', '2020-02-15 13:04:17', '2020-02-15 13:04:17', 4, 21);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `has_viewed` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `message`, `created_at`, `updated_at`, `user_id`, `has_viewed`) VALUES
(1, 'Burim nese mundesh me rregullu kete problem...', '2020-02-09 14:41:03', '2020-02-09 15:56:05', 2, 1),
(2, 'A task is assigned to you.', '2020-02-09 14:50:12', '2020-02-09 16:03:35', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0243605add69b77d3854720901172257c9bee57d59104b2947c43931283559fc8d3ad50aa9712aa1', 2, 1, 'rrotrackToken', '[]', 1, '2020-01-22 19:37:06', '2020-01-22 19:37:06', '2021-01-22 20:37:06'),
('09e643b56fe03da391bac8b3b66c8fa9ac292ba08eb8588599d61f6019e6fa57953cef7114acb04a', 2, 1, 'rrotrackToken', '[]', 0, '2020-01-25 12:00:08', '2020-01-25 12:00:08', '2021-01-25 13:00:08'),
('15d0e93f043236a66ad1b89705724e93b5f3f8efc215457aecc843fc2fc89e69de3449f471161a4d', 4, 1, 'rrotrackToken', '[]', 1, '2020-02-13 22:00:17', '2020-02-13 22:00:17', '2021-02-13 23:00:17'),
('2197f206375a2f452206439b1445a6d8c9dc0f6039fb824130442b507395bcfb7f55e541f139d3ca', 2, 1, 'rrotrackToken', '[]', 1, '2020-01-25 19:33:13', '2020-01-25 19:33:13', '2021-01-25 20:33:13'),
('2994e140bfde7871696984527df05cfb200a6fda1c82bd822b2cae6f6c20b00cb8eb72a2748b077c', 2, 1, 'rrotrackToken', '[]', 1, '2020-01-25 11:52:22', '2020-01-25 11:52:22', '2021-01-25 12:52:22'),
('2abe04c4c568f96fe13d2ad205f1d2443b9086b051539b7de0850b6cebba9ac347b941a3fd1f51e7', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-13 20:40:26', '2020-02-13 20:40:26', '2021-02-13 21:40:26'),
('2b3fa0010eb8347f8264b64ad1e38f2e80e5242e57c904e4fc5e47c0393ec235e9b3342e016a6dd7', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-09 13:30:44', '2020-02-09 13:30:44', '2021-02-09 14:30:44'),
('2e3e5d95d0bfab33c6f6cfe3731af42be4e49fa6e2be207b809f89396a89f9485937310cf6eb453e', 2, 1, 'rrotrackToken', '[]', 1, '2020-01-25 12:19:14', '2020-01-25 12:19:14', '2021-01-25 13:19:14'),
('3627119f5dc23137d7e785d93bdd29ce6d21b7a649e31aa4b4deec78b48c86b3b7074873b0e9f6e4', 2, 1, 'rrotrackToken', '[]', 1, '2020-01-25 11:59:15', '2020-01-25 11:59:15', '2021-01-25 12:59:15'),
('3a645a0f3dbe5542306ee4bfbb8519e53d6f2eed7b78ab86db90dd5ffee7d148cded8f4df758ea89', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-13 20:15:20', '2020-02-13 20:15:20', '2021-02-13 21:15:20'),
('4d29076b93baab592e55ab80e44a8ba53a47bc3ee2a6caacc91632a65497925bdc47eb1fb3ca5431', 4, 1, 'rrotrackToken', '[]', 1, '2020-01-25 19:32:11', '2020-01-25 19:32:11', '2021-01-25 20:32:11'),
('63af07bec66c6db2ab000c5a5e0f2dd828d9447108f6ecbe4c239ffc6dff2dd4b9504ca62bcd12d6', 2, 1, 'rrotrackToken', '[]', 1, '2020-01-22 19:37:21', '2020-01-22 19:37:21', '2021-01-22 20:37:21'),
('6b8769cc7e73dfdac06a6239235afb2f116aa4825f33346d74b0fc24d26bb821e1133a89068336c0', 4, 1, 'rrotrackToken', '[]', 1, '2020-02-13 21:58:20', '2020-02-13 21:58:20', '2021-02-13 22:58:20'),
('73006a1dca3857e177bfb9445ca6781dad367da485adde74a51c60a0b23bcbf99bc7fc1af6f3d102', 2, 1, 'rrotrackToken', '[]', 0, '2020-01-25 12:00:17', '2020-01-25 12:00:17', '2021-01-25 13:00:17'),
('94e61a76d196c9f88863d1bb7410b77adfd1b3e69dcd3a57a0dec939d47aa33efb1fbcf25db00d1f', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-17 11:01:54', '2020-02-17 11:01:54', '2021-02-17 12:01:54'),
('9753bde6150687d3888750f93ffc835b03ad8f398c8f535aa8d28bf90d93672f2da5ec202db8a015', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-09 13:34:24', '2020-02-09 13:34:24', '2021-02-09 14:34:24'),
('98eb84cdc0179b5ac919a253fa3b83a04bc67f3b8485285950ef3d7205fdc546fe3dafdbb041d6ec', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-13 22:05:12', '2020-02-13 22:05:12', '2021-02-13 23:05:12'),
('a8c42ff297b45bc14c212d957f6b9cfc01052b9828e2fed01a6fcd09e2bfea5d1cb34270d6119f31', 2, 1, 'rrotrackToken', '[]', 1, '2020-01-21 20:51:37', '2020-01-21 20:51:37', '2021-01-21 21:51:37'),
('adcc0515386a79f121e906671d7efd15e7c2a0db99e06e49d65b0a0306ed0118462865ee096bb633', 2, 1, 'rrotrackToken', '[]', 0, '2020-02-18 17:54:15', '2020-02-18 17:54:15', '2021-02-18 18:54:15'),
('c3ec831905e336a2936be3cf445712271a10b284362855e02815f5e4f542dd16dc52d87827330bac', 2, 1, 'rrotrackToken', '[]', 1, '2020-01-22 19:37:54', '2020-01-22 19:37:54', '2021-01-22 20:37:54'),
('d0875eb6d3a2020f64d65ad6f6b9636dc4153430cdb1684c6962ab787c560e3a66dc21d51a36ba31', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-18 17:49:43', '2020-02-18 17:49:43', '2021-02-18 18:49:43'),
('d0c423b92391ddf76de27a91af8b6799eb59a993debd3c2c960d53835b8940b62c26fe8541c4ce25', 4, 1, 'rrotrackToken', '[]', 1, '2020-02-13 22:10:30', '2020-02-13 22:10:30', '2021-02-13 23:10:30'),
('d34a0f924c8a9b9241c1ef03cf95b798b48362c42c46baf4649e397e81a8ef7b0bde1ecbd0b214b2', 4, 1, 'rrotrackToken', '[]', 1, '2020-02-14 18:08:08', '2020-02-14 18:08:08', '2021-02-14 19:08:08'),
('e9272d14aafcf0250ab9866c98bdbf20b6ee4c3f25668ae3b9a28100654b821a4ca7b3f937bab182', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-13 22:05:41', '2020-02-13 22:05:41', '2021-02-13 23:05:41'),
('ea30cf9f34bd0f4412e834c82c2d4036c69bf6b45c99addddf751a018f2ad45138399ba48cd25522', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-13 22:10:54', '2020-02-13 22:10:54', '2021-02-13 23:10:54'),
('f3315d9fc505dc5c5c1536f37f3c7b8c9933755dfbe26086d25bd4ad0466281bcfad5ec047bc2aea', 2, 1, 'rrotrackToken', '[]', 1, '2020-01-25 12:00:39', '2020-01-25 12:00:39', '2021-01-25 13:00:39'),
('fcb5193e08333372bcdc63b6abe8f1505e7ea63a878d8e756e4932678920fe772444e383849dfe8d', 2, 1, 'rrotrackToken', '[]', 1, '2020-02-09 13:55:53', '2020-02-09 13:55:53', '2021-02-09 14:55:53');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'XCTOmqJHCqL00RjbMRSaPWmhTdnmJXA0tg9kw7jO', 'http://localhost', 1, 0, 0, '2020-01-19 11:23:42', '2020-01-19 11:23:42'),
(2, NULL, 'Laravel Password Grant Client', 'vlEPa8iOSAekmlVn5aVZ1B3wH2hqnLg3YKLv2AkJ', 'http://localhost', 0, 1, 0, '2020-01-19 11:23:42', '2020-01-19 11:23:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-01-19 11:23:42', '2020-01-19 11:23:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deadline` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `deadline`, `created_at`, `updated_at`, `logo`) VALUES
(1, 'Ipko', '2020-05-27 22:00:00', '2020-01-21 21:19:31', '2020-01-21 21:19:31', NULL),
(3, 'Safety portal', '2020-01-31 23:00:00', '2020-01-21 21:19:31', '2020-01-21 21:19:31', NULL),
(4, 'Deepsea', '2019-10-22 22:00:00', '2020-01-21 21:48:06', '2020-01-21 21:48:06', NULL),
(5, 'Wertheim', '2020-04-16 22:00:00', '2020-01-21 21:48:06', '2020-01-21 21:48:06', NULL),
(6, 'Iknow', '2020-02-13 23:00:00', '2020-01-21 21:48:06', '2020-01-21 21:48:06', NULL),
(9, 'Rrota track', '2020-04-24 22:00:00', '2020-01-21 21:48:06', '2020-01-21 21:48:06', NULL),
(10, 'Class', '2020-02-28 23:00:00', '2020-01-21 21:48:06', '2020-01-21 21:48:06', NULL),
(14, 'RVF - Der Regio-Verkehrsverbund Freiburg', '2020-03-30 22:00:00', '2020-02-17 12:00:36', '2020-02-17 14:57:41', '5e4ab7f5b7a0c.svg'),
(15, 'Albaqeramika', '2020-02-28 23:00:00', '2020-02-17 12:06:44', '2020-02-17 12:06:44', '5e4a8fe4e5c8a.svg');

-- --------------------------------------------------------

--
-- Table structure for table `project_user`
--

CREATE TABLE `project_user` (
  `project_id` bigint(20) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project_user`
--

INSERT INTO `project_user` (`project_id`, `user_id`) VALUES
(1, 2),
(1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Administrator'),
(2, 'Project Manager'),
(3, 'Designer'),
(4, 'Developer'),
(5, 'Visitor');

-- --------------------------------------------------------

--
-- Table structure for table `sprints`
--

CREATE TABLE `sprints` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `end_date` date NOT NULL,
  `project_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sprints`
--

INSERT INTO `sprints` (`id`, `name`, `start_date`, `end_date`, `project_id`) VALUES
(1, 'Sprint #SYjO', '2020-01-22 23:00:00', '2020-03-25', 1),
(2, 'Sprint(I)-hkY', '2020-01-03 23:00:00', '2020-01-18', 1),
(3, 'Sprint(I)-HfL', '2019-12-06 23:00:00', '2019-12-21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `estimate` bigint(20) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `task_role_id` bigint(20) NOT NULL,
  `column_id` bigint(20) DEFAULT NULL,
  `user_story_id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `description`, `estimate`, `image`, `display_order`, `user_id`, `task_role_id`, `column_id`, `user_story_id`, `project_id`) VALUES
(21, 'Design login form', 'Curabitur aliquet quam id dui posuere blandit. Donec rutrum congue leo eget malesuada.', 2, '5e4800ee0e50a.jpeg', NULL, 3, 1, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `task_roles`
--

CREATE TABLE `task_roles` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `task_roles`
--

INSERT INTO `task_roles` (`id`, `name`) VALUES
(1, 'Design'),
(2, 'Backend'),
(3, 'Frontend');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `bio`, `email`, `avatar`, `cover`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`) VALUES
(2, 'Burim Dervisholli', 'Proin eget tortor risus. Cras ultricies ligula sed magna dictum portadine. test', 'dervisholliburim@gmail.com', '5e28aa386d9ca.png', '5e28af2b6a79f.jpg', NULL, '$2y$10$f38NekXG/vvtID71oAUvLePtq1PuP55HJ9ZqPU/YmJdqHXGsHxLyi', NULL, '2020-01-19 16:18:45', '2020-02-13 20:15:40', 4),
(3, 'Filan Fisteku', NULL, 'filanfisteku@gmail.com', NULL, NULL, NULL, '$2y$10$nzioZg6G52mdR/2H/GOO4uFgPO.J2xrn3PbA4dqUA8Ul8nM8TRaQO', NULL, '2020-01-23 19:32:04', '2020-01-23 19:32:04', 3),
(4, 'Jane Doe', 'Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit.', 'jane@gmail.com', '5e2ca5f8da425.jpg', '5e2ca5f8da655.jpg', NULL, '$2y$10$coU6DMNg8DASdZBZVUj94up4rtYfDMjjxd.lhITuUBw3DtIK72kKi', NULL, '2020-01-23 21:54:06', '2020-01-25 19:32:56', 4);

-- --------------------------------------------------------

--
-- Table structure for table `user_stories`
--

CREATE TABLE `user_stories` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `size` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `project_id` bigint(20) NOT NULL,
  `sprint_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_stories`
--

INSERT INTO `user_stories` (`id`, `name`, `size`, `created_at`, `updated_at`, `project_id`, `sprint_id`) VALUES
(1, 'As a user I want to be able to login into system', 'Large', '2020-01-22 22:06:58', '2020-01-22 22:06:58', 1, NULL),
(2, 'As an administrator I want to be able to see the dashboard menu', '0', '2020-01-22 22:06:58', '2020-01-22 22:06:58', 1, NULL),
(3, 'As an system administrator I want to be able to see user logs to keep track easier', '0', '2020-01-22 23:31:49', '2020-01-22 23:31:49', 1, NULL),
(4, 'As a user I want to see the latest Iphone in the home page', 'Medium', '2020-01-23 20:19:13', '2020-01-23 20:19:13', 1, NULL),
(5, 'As an administrator I want to see some statistics about my page', 'Large', '2020-01-23 20:21:37', '2020-01-23 20:21:37', 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `columns`
--
ALTER TABLE `columns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sprint_column_id` (`sprint_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_user_fk` (`user_id`),
  ADD KEY `comment_task_fk` (`task_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_user_fk` (`user_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_user`
--
ALTER TABLE `project_user`
  ADD KEY `project_user_fk` (`project_id`),
  ADD KEY `user_project_fk` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sprints`
--
ALTER TABLE `sprints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sprint_project_fk` (`project_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_task_fk` (`user_id`),
  ADD KEY `taskrole_task__fk` (`task_role_id`),
  ADD KEY `column_task_fk` (`column_id`),
  ADD KEY `user_story_task_fk` (`user_story_id`),
  ADD KEY `project_task_fk` (`project_id`);

--
-- Indexes for table `task_roles`
--
ALTER TABLE `task_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `user_role_fk` (`role_id`);

--
-- Indexes for table `user_stories`
--
ALTER TABLE `user_stories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_story_project_fk` (`project_id`),
  ADD KEY `user_story_sprint_fk` (`sprint_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `columns`
--
ALTER TABLE `columns`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sprints`
--
ALTER TABLE `sprints`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `task_roles`
--
ALTER TABLE `task_roles`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_stories`
--
ALTER TABLE `user_stories`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `columns`
--
ALTER TABLE `columns`
  ADD CONSTRAINT `sprint_column_id` FOREIGN KEY (`sprint_id`) REFERENCES `sprints` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comment_task_fk` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_user_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notification_user_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project_user`
--
ALTER TABLE `project_user`
  ADD CONSTRAINT `project_user_fk` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `user_project_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `sprints`
--
ALTER TABLE `sprints`
  ADD CONSTRAINT `sprint_project_fk` FOREIGN KEY (`project_id`) REFERENCES `sprints` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `column_task_fk` FOREIGN KEY (`column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_task_fk` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `taskrole_task__fk` FOREIGN KEY (`task_role_id`) REFERENCES `task_roles` (`id`),
  ADD CONSTRAINT `user_story_task_fk` FOREIGN KEY (`user_story_id`) REFERENCES `user_stories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_task_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_role_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_stories`
--
ALTER TABLE `user_stories`
  ADD CONSTRAINT `user_story_project_fk` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_story_sprint_fk` FOREIGN KEY (`sprint_id`) REFERENCES `sprints` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
