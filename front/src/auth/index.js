export const auth = {
    isAuthenticated(){
        if(this.getToken()){
            return true
        }else{
            return false
        }
    },    
    setToken(token) {
        localStorage.setItem('token', token)
    },
    setUser(user) {
        localStorage.setItem('user', JSON.stringify(user))
    }, 
    getToken() {
        var token = localStorage.getItem('token')
        return token
    },
    getUser(){
        var user = JSON.parse(localStorage.getItem('user'))
        return user
    }
}