import Vue from 'vue'
import VueRouter from 'vue-router'
import { auth } from './../auth'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/',
    component: () => import('../views/Dashboard.vue'),    
    children: [
      {
        path: '/',
        name: 'home',
        component: () => import('../views/Home.vue')
      },
      {
        path: '/profile',
        name: 'profile',
        component: () => import('../views/Profile.vue')
      },
      {
        path: '/notifications',
        name: 'notifications',
        component: () => import('../views/Notifications.vue')
      },
      {
        path: '/users',
        name: 'users',
        component: () => import('../views/Users.vue')
      },
      {
        path: '/users/:id',
        name: 'userDetail',
        component: () => import('../views/UserDetail.vue')
      },
      {
        path: '/projects',
        name: 'projects',
        component: () => import('../views/Projects.vue')
      },
      {
        path: '/projects/new',
        name: 'newProject',
        component: () => import('../views/NewProject.vue')
      },
      {
        path: '/projects/:id',
        name: 'projectDetail',
        component: () => import('../views/ProjectDetail.vue')
      },
      {
        path: '/projects/update/:id',
        name: 'projectUpdate',
        component: () => import('../views/ProjectUpdate.vue')
      },   
      {
        path: '/sprints/:id',
        name: 'sprintDetail',
        component: () => import('../views/SprintDetail.vue')
      },
      {
        path: '/userstories/:id',
        name: 'userStoryDetail',
        component: () => import('../views/UserStoryDetail.vue')
      },
      {
        path: '/tasks/:id',
        name: 'taskDetails',
        component: () => import('../views/TaskDetail.vue')
      },
    ]
  }  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (!auth.isAuthenticated() && to.path !== '/login') {
    next({ path: '/login' })
  } else {
    next()
  }
}) 

export default router
