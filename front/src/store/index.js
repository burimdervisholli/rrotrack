import Vue from 'vue'
import Vuex from 'vuex'
import { auth } from './../auth'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: [],
    user:  auth.isAuthenticated() ? auth.getUser() : [],
    token: auth.isAuthenticated() ? auth.getToken() : null,
    isAuthenticated: auth.isAuthenticated(),
    baseImageUrl: 'http://127.0.0.1:8000/public/',
    avatarsImageUrl: 'http://127.0.0.1:8000/uploads/avatars/',
    coversImageUrl: 'http://127.0.0.1:8000/uploads/covers/',
    projectsImageUrl: 'http://127.0.0.1:8000/uploads/project_logos/',
    commentsImageUrl: 'http://127.0.0.1:8000/uploads/comment_images/',
    tasksImageUrl: 'http://127.0.0.1:8000/uploads/task_images/',
    colors: ['red darken-1', 'purple lighten-4', 'purple darken-4', 'pink darken-2', 'deep-purple darken-1', 'deep-purple darken-4', 'indigo darken-1', 'indigo darken-4', 'blue darken-4', 'light-blue lighten-4', 'light-blue darken-1', 'light-blue darken-4', 'cyan darken-2', 'cyan darken-4', 'teal darken-1', 'teal darken-4', 'green darken-1', 'green darken-4', 'amber darken-1', 'amber darken-4', 'orange darken-1', 'orange darken-4', 'blue-grey darken-1', 'blue-grey darken-4']
  },
  mutations: {
    setUsers(state, payload) {
      state.users = payload
    },
    setUser(state, payload) {
      state.user = payload
    },
    setAuth(state, payload) {
      state.isAuthenticated = payload
    },
    setToken(state, payload) {
      state.token = payload
    }
  },
  actions: {
    setUsers(context, payload) {
      context.commit('setUsers', payload)
    },
    setUser(context, payload) {
      context.commit('setUser', payload)
    },
    setAuth(context, payload) {
      context.commit('setAuth', payload)
    },
    setToken(context, payload) {
      context.commit('setToken', payload)
    }
  },
  getters: {
    users(state) {
      return state.users
    },
    user(state) {
      return state.user
    },
    isAuthenticated(state) {
      return state.isAuthenticated
    },
    token(state) {
      return state.token
    },
    baseImageUrl(state) {
      return state.baseImageUrl
    },
    coversImageUrl(state) {
      return state.coversImageUrl
    },
    avatarsImageUrl(state) {
      return state.avatarsImageUrl
    },
    projectsImageUrl(state) {
      return state.projectsImageUrl
    },
    commentsImageUrl(state) {
      return state.commentsImageUrl
    },
    tasksImageUrl(state) {
      return state.tasksImageUrl
    },
    colors(state) {
      return state.colors
    }
  }
})
